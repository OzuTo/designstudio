'use strict';
function slider(options){
	this.ELEM = options.elem;
	//номер текущей картинки
	this.current = 1;
	
	//список картинок
	this.UL = options.elem.find('div.slideImage')
				.css({'overflow': 'hidden'})
				.children('ul');
	//коллекция картинок
	this.img = this.UL.find('img');
	//размер каринки
	this.imgW = this.ELEM.find('div.slideImage').width();
	//количество картинок
	this.imgL = this.img.length;
	//ширина всех картинок
	this.TotalImgW = this.imgW*this.imgL;
}
slider.prototype.doIt = function(position, direction){
	let sign; //-= +=
		
		if(direction && position !== 0){
			sign = (direction === 'next') ? '-=' : '+='; 
		}
		
		this.UL.animate({'margin-left': sign ? (sign+position): position}, 2500);
};
slider.prototype.clickButton = function(direction){
	//console.log(direction);
	let position = this.imgW;
	(direction === 'next')? ++this.current : --this.current;
			
		if(this.current === 0){
		 	this.current =this.imgL;
		 	direction = 'next'
		 	position = this.TotalImgW-this.imgW;
		 }else if(this.current-1 === this.imgL){
		 	this.current = 1;
		 	position = 0;
		 }
		 
		 this.doIt(position,direction);
};
slider.prototype.init = function(){
	//кнопки
	let btn =this.ELEM.find('div.button'),
	obj = this;
	btn.on('click', function(){
		obj.clickButton($(this).attr('id'));
	});

};



function initSlideShow(){
	var slideShow = new slider({
		elem: $('div.slideShow')
	});
	slideShow.init();
}