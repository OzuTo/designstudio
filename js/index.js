'use strict';
//функция конструктор навигационного меню
function navigation(options){
	//свойства
	this._elem = options.elem;
}
	//методы
	//создание XmlHttpRequest
navigation.prototype.getXmlHttpRequest = function(){
	if (window.XMLHttpRequest) 
		{
			try 
			{
				return new XMLHttpRequest();
			} 
			catch (e){}
		} 
		else if (window.ActiveXObject) 
		{
			try 
			{
				return new ActiveXObject('Msxml2.XMLHTTP');
			} catch (e){}
			try 
			{
				return new ActiveXObject('Microsoft.XMLHTTP');
			} 
			catch (e){}
		}
		return null;
};
// подгрука контента по ссылке
navigation.prototype.loadLink = function(link, async){
	let xhr = this.getXmlHttpRequest();
	if(async){
		xhr.timeout = 30000;
		xhr.ontimeout = function(){
			alert('The request has expired!');
		}
	}
		xhr.onreadystatechange = function(){
			if(xhr.readyState == 4){
				if(xhr.status != 200){
					console.log("Статус: " + xhr.status + " " + xhr.statusText);
				}else{
					
					$('article').html(xhr.responseText);
					switch (link){
						case './about.html':{
							initSlideShow();
							break;
						}
						case './gallery.html':{
							$('ul#G_columL li a.photo, ul#G_columR li a.photo').fancybox();
							break;
						}
					}
					
				
				}
			}
		}
		xhr.open('POST', link, async);
		xhr.send(null);
};
//инициализвция меню
navigation.prototype.init = function(event){
	let thisObj = this;
	thisObj._elem.addEventListener('click', function(e){
		let target = e.target;
		thisObj.loadLink(target.getAttribute('data-href'), true);
	});
};
//возвращает dom меню
navigation.prototype.getElem = function(){
	return this._elem;
};


//меню ссылок на соц сети в footer
function menuLink(options){
	this._LI = options.LI;
	this.linksImg = [
		{src: './img/icon1.jpg'},
		{src: './img/icon2.jpg'},
		{src: './img/icon3.jpg'},
		{src: './img/icon4.jpg'},
		{src: './img/icon5.jpg'},
	];
}
//методы
menuLink.prototype.addImg = function(){
	let LI = this._LI;
	$.each(this.linksImg,function(key, val){
		LI.eq(key).css({'background-image':'url("'+val.src+'")'});
	});
};
menuLink.prototype.getElem = function(){
	return this._LI;
};


//форма newsletter
function toFormContact(options){
	navigation.apply(this, arguments);
}
toFormContact.prototype = Object.create(navigation.prototype);
toFormContact.prototype.constructor = toFormContact;

toFormContact.prototype.init = function(){
	let thsObj = this;
	this._elem.find('p').on('click', function(){
		thsObj.clickContact();
	});
};
toFormContact.prototype.clickContact = function(){
	let email = this._elem.find('input[type = "text"]').val();
	this.loadLink('./contacts.html', false);
	document.getElementById('scroll').scrollIntoView(true);
	$('input[name = "email"]').val(email);

};


$(function(){
//navigation menu in index.html
	var menu = new navigation({
		elem: document.getElementById('navigationMenu')
	});
	//инициализация меню
	menu.init();
	//подгрузка первой страницы
	menu.loadLink(menu.getElem().firstElementChild.getAttribute('data-href'));

	var followUs = new menuLink({
		LI: $('footer div.links').children().last().children().first().children()
	});
	followUs.addImg();

	var newsletter = new toFormContact({
		elem: $('footer div.newsletter')
	});

	newsletter.init();
});